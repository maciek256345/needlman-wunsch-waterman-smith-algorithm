import math
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt


# Funkcja zapisująca alignmenty do pliku tekstowego, wraz z parametrami
def write_alignments_to_txt_file(alignment1, alignment2, gap, match, mismatch, identic, breaks, total):
    identic = identic / (total) * 100
    breaks = breaks / (total) * 100
    identic = np.round(identic, 2)
    breaks = np.round(breaks, 2)

    file = open("results.txt", "w")
    file.write("PARAMETERS:")
    file.write("\n")
    file.write(("Gap: " + str(gap) + ", match: " + str(match) + ", mismatch: " + str(mismatch) +
                "\nIdentic positions (%): " + str(identic) + ", breaks (%): " + str(breaks)))
    file.write("\n")
    file.write("\n")
    file.write("ALIGNMENTS:")
    file.write("\n")
    file.write("Alignemnt 1: " + alignment1)
    file.write("\n")
    file.write("Alignemnt 2: " + alignment2)
    file.close()


# Funkcja prezentująca punktację ze ścieżką w postaci graficznej
def display_matrix(matrix, gap, match, mismatch, identic, breaks, total):
    identic = identic / (total) * 100
    breaks = breaks / (total) * 100
    identic = np.round(identic, 2)
    breaks = np.round(breaks, 2)

    ax = sns.heatmap(matrix, cmap='magma')
    plt.xticks([])
    plt.xlabel("Sequence 2")
    plt.yticks([])
    plt.ylabel("Sequence 1")
    plt.title("Gap: " + str(gap) + " | Match: " + str(match) + " | Mismatch: " + str(mismatch) +
              "\nIdentic positions (%): " + str(identic) + " | Breaks (%): " + str(breaks))

    plt.show()


# Funkcja sprawdzająca poprawność danych wejściowych
def check_correctness(sequence):
    # Zbiór zasad azotowych oraz jednoliterowych nazw aminokwasów
    set = ["A", "C", "G", "T", "R", "N", "D", "B", "Q",
           "E", "Z", "H", "I", "L", "K", "M", "F",
           "P", "S", "W", "Y", "V"]

    # Sprawdzenie poprawności wprowadzonej sekwencji
    for i in range(len(sequence)):
        if sequence[i] not in set:
            raise ValueError("Niepoprawna sekwencja")


# Funkcja wczytująca sekwencję z pliku .fasta
def open_from_fasta_file(path):
    x = open(path, "r")
    lines = x.readlines()
    lines.remove(lines[0])
    sequence_list = []
    sequence = ""

    # dodanie linii do listy i usunięcie znaku nowej linii
    # uzyskanie listy z ciagla sekwencja
    for line in lines:
        sequence_list += line
        sequence_list.remove("\n")

    # zamiana listy na string (w całości, nie w kilku liniach)
    for i in range(len(sequence_list)):
        sequence += sequence_list[i]
    return sequence


def find_path_alignments_needleman(seq1, seq2, letter_matrix, matrix, gap, match, mismatch):
    path_matrix = np.zeros((len(seq1), len(seq2)))
    path_with_scores = np.copy(matrix)

    identic = 0
    breaks = 0
    al1 = ""
    al2 = ""

    minimum = math.inf
    for w in range(len(seq1)):
        for k in range(len(seq2)):
            if matrix[w][k] < minimum:
                minimum = matrix[w][k]

    w = len(seq1) - 1
    k = len(seq2) - 1

    path_matrix[w][k] = 1
    path_with_scores[w][k] = minimum

    while letter_matrix[w][k] != '0':
        if letter_matrix[w][k] == "D":
            al1 += seq1[w]
            al2 += seq2[k]
            w -= 1
            k -= 1
            identic += 2
        elif letter_matrix[w][k] == "L":
            al1 += "-"
            al2 += seq2[k]
            k -= 1
            identic += 1
            breaks += 1
        else:
            al1 += seq1[w]
            al2 += "-"
            w -= 1
            identic += 1
            breaks += 1
        path_matrix[w][k] = 1
        path_with_scores[w][k] = minimum

    al1 = al1[::-1]
    al2 = al2[::-1]

    total = len(al1) + len(al2)

    display_matrix(path_matrix, gap, match, mismatch, identic, breaks, total)
    display_matrix(path_with_scores, gap, match, mismatch, identic, breaks, total)
    write_alignments_to_txt_file(al1, al2, gap, match, mismatch, identic, breaks, total)

    print('Alignment 1: ' + al1)
    print('Alignment 2: ' + al2)


def needlman_wunsch_algorithm(s1_or_path, s2_or_path, gap, match, mismatch, is_from_file):
    if is_from_file:
        seq1 = open_from_fasta_file(s1_or_path)
        seq2 = open_from_fasta_file(s2_or_path)
    else:
        seq1 = s1_or_path
        seq2 = s2_or_path

    # Zamiana wszystkich liter na duże
    seq1 = seq1.upper()
    seq2 = seq2.upper()

    # Sprawdzenie poprawności danych wejściowych
    check_correctness(seq1)
    check_correctness(seq2)

    print("Sequence 1 (rows): " + seq1)
    print("Sequence 2 (columns): " + seq2)
    print("-----------------------------------------------")
    seq1 = "-" + seq1
    seq2 = "-" + seq2
    matrix = np.zeros((len(seq1), len(seq2)))
    letter_matrix = np.full((len(seq1), len(seq2)), "0")
    letter = ""

    for k in range(len(seq2)):
        if k != 0:
            matrix[0][k] = matrix[0][k - 1] + gap
            letter_matrix[0][k] = 'L'

    for w in range(len(seq1)):
        if w != 0:
            matrix[w][0] = matrix[w - 1][0] + gap
            letter_matrix[w][0] = 'U'

    w = 1
    while w < len(seq1):
        k = 1
        while k < len(seq2):
            L = matrix[w][k - 1] + gap
            U = matrix[w - 1][k] + gap
            if seq1[w] == seq2[k]:
                what = match
            else:
                what = mismatch
            D = matrix[w - 1][k - 1] + what
            x = [L, U, D]
            maximum = np.max(x)

            if maximum == L:
                letter = 'L'
            elif maximum == U:
                letter = 'U'
            else:
                letter = 'D'
            matrix[w][k] = maximum
            letter_matrix[w][k] = letter
            k += 1
        w += 1

    find_path_alignments_needleman(seq1, seq2, letter_matrix, matrix, gap, match, mismatch)


def find_path_alignments_waterman(seq1, seq2, letter_matrix, matrix, gap, match, mismatch):
    max_w = 0
    max_k = 0
    maximum = -math.inf

    path_with_scores = np.copy(matrix)
    path_matrix = np.zeros((len(seq1), len(seq2)))

    identic = 0
    breaks = 0

    for w in range(len(seq1)):
        for k in range(len(seq2)):
            if matrix[w][k] > maximum:
                maximum = matrix[w][k]
                max_w = w
                max_k = k

    al1 = ""
    al2 = ""

    w = max_w
    k = max_k

    path_matrix[w][k] = 1
    path_with_scores[w][k] = maximum

    while matrix[w][k] != 0:
        if letter_matrix[w][k] == "D":
            al1 += seq1[w]
            al2 += seq2[k]
            w -= 1
            k -= 1
            identic += 2
        elif letter_matrix[w][k] == "L":
            al1 += "-"
            al2 += seq2[k]
            k -= 1
            identic += 1
            breaks += 1
        else:
            al1 += seq1[w]
            al2 += "-"
            w -= 1
            identic += 1
            breaks += 1

        path_matrix[w][k] = 1
        path_with_scores[w][k] = maximum

    al1 = al1[::-1]
    al2 = al2[::-1]
    al11 = seq1[1:w + 1] + "*[" + al1 + "]*" + seq1[max_w + 1:]
    al22 = seq2[1:k + 1] + "*[" + al2 + "]*" + seq2[max_k + 1:]

    total = len(al11) + len(al22) - 8

    display_matrix(path_matrix, gap, match, mismatch, identic, breaks, total)
    display_matrix(path_with_scores, gap, match, mismatch, identic, breaks, total)
    write_alignments_to_txt_file(al11, al22, gap, match, mismatch, identic, breaks, total)

    print('Alignment 1: ' + al11)
    print('Alignment 2: ' + al22)


def waterman_smith_algorithm(s1_or_path, s2_or_path, gap, match, mismatch, is_from_file):
    if is_from_file:
        seq1 = open_from_fasta_file(s1_or_path)
        seq2 = open_from_fasta_file(s2_or_path)
    else:
        seq1 = s1_or_path
        seq2 = s2_or_path

    # Zamiana wszystkich liter na duże
    seq1 = seq1.upper()
    seq2 = seq2.upper()

    # Sprawdzenie poprawności danych wejściowych
    check_correctness(seq1)
    check_correctness(seq2)

    print("Sequence 1 (rows): " + seq1)
    print("Sequence 2 (columns): " + seq2)
    print("-----------------------------------------------")
    seq1 = "-" + seq1
    seq2 = "-" + seq2
    matrix = np.zeros((len(seq1), len(seq2)))
    letter_matrix = np.full((len(seq1), len(seq2)), "0")

    for k in range(len(seq2)):
        if k != 0:
            matrix[0][k] = 0
            letter_matrix[0][k] = 'L'

    for w in range(len(seq1)):
        if w != 0:
            matrix[w][0] = 0
            letter_matrix[w][0] = 'U'

    w = 1
    while w < len(seq1):
        k = 1
        while k < len(seq2):
            L = matrix[w][k - 1] + gap
            U = matrix[w - 1][k] + gap
            if seq1[w] == seq2[k]:
                what = match
            else:
                what = mismatch
            D = matrix[w - 1][k - 1] + what
            x = [L, U, D, 0]
            maximum = np.max(x)

            if maximum == L:
                letter = 'L'
            elif maximum == U:
                letter = 'U'
            elif maximum == D:
                letter = 'D'
            else:
                letter = '0'
            matrix[w][k] = maximum
            letter_matrix[w][k] = letter
            k += 1
        w += 1

    find_path_alignments_waterman(seq1, seq2, letter_matrix, matrix, gap, match, mismatch)

# bardzo prosty przyklad
needlman_wunsch_algorithm('GTGAATA', 'ACCGTGA', -1, 1, -1, False)